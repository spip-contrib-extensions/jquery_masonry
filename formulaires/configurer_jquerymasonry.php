<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) { return;
}

include_spip('inc/config');

function formulaires_configurer_jquerymasonry_saisies_dist() {
	$saisies = [];
	$config = lire_config('jquerymasonry');

	$saisies = [
		[
			'saisie' => 'explication',
			'options' => [
				'nom' => 'explication',
				'texte' => _T('jquerymasonry:configurer_explication')
			]
		],
		[
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'fjquerymasonry_nombre',
				'label' => _T('jquerymasonry:legend_jquerymasonry_nombre')
			],
			'saisies' => [
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'nombre',
						'label' => _T('jquerymasonry:label_nombre'),
						'obligatoire' => 'oui',
						'defaut' => $config['nombre']
					],
					'verifier' => [
						'type' => 'entier',
						'options' => [
							'min' => 1,
							'max' => 10
						]
					]
				]
			]
		]
	];

	for ($i = 1; $i <= $config['nombre']; $i++) {
		array_push($saisies, [
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'fjquerymasonry' . $i,
				'label' => _T('jquerymasonry:legend_jquerymasonry', ['numero' => $i])
			],
			'saisies' => [
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'container' . $i,
						'label' => _T('jquerymasonry:label_container'),
						'explication' => _T('jquerymasonry:explication_container'),
						'defaut' => $config['container' . $i]
					]
				],
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'items' . $i,
						'label' => _T('jquerymasonry:label_items'),
						'explication' => _T('jquerymasonry:explication_items'),
						'defaut' => $config['items' . $i]
					]
				],
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'largeur' . $i,
						'label' => _T('jquerymasonry:label_largeur'),
						'explication' => _T('jquerymasonry:explication_largeur'),
						'defaut' => $config['largeur' . $i]
					]
				],
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'marge' . $i,
						'label' => _T('jquerymasonry:label_marge'),
						'explication' => _T('jquerymasonry:explication_marge'),
						'defaut' => $config['marge' . $i]
					]
				],
				[
					'saisie' => 'oui_non',
					'options' => [
						'nom' => 'multicolonne' . $i,
						'label' => _T('jquerymasonry:label_multicolonne'),
						'explication' => _T('jquerymasonry:explication_multicolonne'),
						'defaut' => $config['multicolonne' . $i]
					]
				],
				[
					'saisie' => 'oui_non',
					'options' => [
						'nom' => 'animation' . $i,
						'label' => _T('jquerymasonry:label_animation'),
						'explication' => _T('jquerymasonry:explication_animation'),
						'defaut' => $config['animation' . $i]
					]
				]
			]
		]) ;
	}

	return($saisies);
}
