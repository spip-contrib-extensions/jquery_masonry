<?php

function jquerymasonry_insert_head($flux) {
	$js_acharger = 'jquery.masonry.js';

	$executer = '';
	$styles = '';
	include_spip('inc/config');
	$conf_jquerymasonry = lire_config('jquerymasonry');

	for ($i = 0; $i <= $conf_jquerymasonry['nombre']; $i++) {
		if (isset($conf_jquerymasonry['container' . $i]) and $conf_jquerymasonry['container' . $i]) {
			$largeur = intval($conf_jquerymasonry['largeur' . $i]) + 2 * intval($conf_jquerymasonry['marge' . $i]) + 10;  // 10px pour les bordures eventuelles
			if ($conf_jquerymasonry['multicolonne' . $i] != 'on') {
				$styles .= "\n" . $conf_jquerymasonry['container' . $i] . ' ' . $conf_jquerymasonry['items' . $i] . '{width:' . $conf_jquerymasonry['largeur' . $i] . 'px;margin:' . $conf_jquerymasonry['marge' . $i] . "px;float:left;}\n" ;
			}
			// contournement glitch lancement trop tôt https://masonry.desandro.com/layout.html#imagesloaded
			$executer .= '$("' . $conf_jquerymasonry['container' . $i] . '").imagesLoaded( function() {';
			$executer .= '	$("' . $conf_jquerymasonry['container' . $i] . '").masonry({' ;
			$executer .= "	itemSelector:'" . $conf_jquerymasonry['items' . $i] . "'," ;
			if ($conf_jquerymasonry['multicolonne' . $i] == 'on') {
				$executer .= 'columnWidth:' . $largeur . ',' ;
			}
			$executer .= '	isRTL:' . (lang_dir() == 'rtl' ? 'true' : 'false') . ',' ;
			$executer .= '	isAnimated:' . ($conf_jquerymasonry['animation' . $i] == 'on' ? 'true' : 'false') ;
			$executer .= '	});' ;
			$executer .= '});' ;
		}
	}

	// S'il y a au moins un element
	if ($conf_jquerymasonry['nombre'] > 0) {
		$flux .= "\n" . '<script src="' . url_absolue(find_in_path('javascript/' . $js_acharger)) . '" type="text/javascript"></script>';
		$flux .= "\n" . '<script src="' . url_absolue(find_in_path('javascript/imagesloaded.pkgd.min.js')) . '" type="text/javascript"></script>';
		if (isset($conf_jquerymasonry['multicolonne' . $i]) and $conf_jquerymasonry['multicolonne' . $i] != 'on') {
			$flux .= "\n" . '<style type="text/css">' . $styles . '</style>';
		}
		$flux .= "\n" . '
<script type="text/javascript">/* <![CDATA[ */
	jQuery(document).ready(function(){
		function jquerymasonry_init(){
			' . $executer . '
		}
		jquerymasonry_init();
		if(typeof onAjaxLoad == "function") onAjaxLoad(jquerymasonry_init);
	});
/* ]]> */</script>
';
	}
	return $flux;
}
