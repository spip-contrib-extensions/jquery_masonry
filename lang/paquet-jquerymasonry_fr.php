<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-jquerymasonry
// Langue: fr
// Date: 21-01-2012 10:48:45
// Items: 2

return [
// J
	'jquerymasonry_description' => 'Une adaptation pour Spip du plugin [JQuery Masonry->http://masonry.desandro.com/]',
	'jquerymasonry_slogan' => 'Le plugin qui range vos blocs',
];
